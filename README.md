# Tof's config for some LaTeX packages

## Content

This project contains some LaTeX style files to configure packages or
define new commands:

- `my-algo.sty` contains some french keywords like Entrées etc for the
  [algorithm2e](https://www.ctan.org/pkg/algorithm2e "Link to CTAN
  algorithm2e page") package.
- `my-env.sty` contains some environments, mainly to emphasize content
  in lab sessions texts for instance.
- `my-listings.sty` contains custom languages definition for the
  [listings](https://www.ctan.org/pkg/listings "Link to CTAN listings
  page") package. It also contains a command to use overlays to
  highlight source code in [Beamer](https://www.ctan.org/pkg/beamer
  "Link to CTAN Beamer page").
- `my-symbols.sty` contains some symbols definition.
- `my-theorems.sty` contains new theorem environments.
- `my-tikz.sty` contains new [TikZ](https://www.ctan.org/pkg/pgf "Link
  to CTAN TikZ page") commands to create E/R diagrams, charts etc.

## Installing

To install the files, execute

```bash
make install
```

This should install the files in your local TeXMF directory
(`~/.local/share/texmf` by default on Linux).

The `hooks` repository contains a simple hook to be linked to
`.git/hooks/post-commit` for instance to install the files when you
commit changes.
